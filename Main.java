import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    private static final String INPUT_FILE = "D:\\Work\\MusicAndPicture\\src\\inFile.txt";
    private static String musicLink, musicFile;
    private static String pictureLink, pictureFile;

    public static void main(String[] args) {
        readInputFile();

        Thread musicThread = new DownloadingThread(musicLink, musicFile);
        musicThread.start();

        Thread pictureThread = new DownloadingThread(pictureLink, pictureFile);
        pictureThread.start();

        try {
            musicThread.join();
            pictureThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        play();
    }

    /**
     * Выполняет чтение исходного файла и сохраняет полученные данные в поля класса
     * <p>
     * Исходный файл состоит из двух строк
     * <ссылка на песню> <путь к песне>
     * <ссылка на картинку> <путь к картинке>
     */
    public static void readInputFile() {

        try (BufferedReader reader = new BufferedReader(new FileReader(INPUT_FILE))) {

            String[] temp = reader.readLine().split(" ");
            musicLink = temp[0];
            musicFile = temp[1];


            temp = reader.readLine().split(" ");
            pictureLink = temp[0];
            pictureFile = temp[1];
        } catch (IOException ioe) {
            //все плохо
            ioe.printStackTrace();
        }
    }

    /**
     * Проигрываем композицию
     */
    public static void play() {
        try (FileInputStream stream = new FileInputStream(musicFile)) {
            Player player = new Player(stream);
            //погнали
            player.play();
        } catch (JavaLayerException e) {
            //все плохо
            e.printStackTrace();
        } catch (IOException e1) {
            //все плохо
            e1.printStackTrace();
        }
    }

}

